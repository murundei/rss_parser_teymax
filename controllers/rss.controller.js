const { to, error, success } = require('../utils/requestHelpers');
const Parser = require('rss-parser');
const RssModel = require("../models/rss.model");

const find_all = async (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  const { body } = req;
  let err, all_rss;
  [err, all_rss]= await to(RssModel.find({}));
  if (err) return error(res, err.message, 400);
  return success(res, all_rss);
};

const find_one = async (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  const { body } = req;
  let err, rss;
  [err, rss] = await to(RssModel.findById(req.body.id));
  if (err) return error(res, err.message, 400);
  return success(res, rss);
};

const rss_save = async (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  const { body } = req;
  let parser = new Parser();
  let parsedUrl = req.body.feedUrl;
  console.log(parsedUrl);
  let feed = await parser.parseURL(parsedUrl);
  const rss = new RssModel({
    url: parsedUrl,
    title: feed.title,
    description: feed.description,
    link: feed.link,
    items: feed.items
  });
  try {
    await rss.save();
    return success(res, rss)
  } catch (err) {
    return error(res, err.message, 400);
  }
};

const rss_delete = async (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  const { body } = req;
  try {
    const rss = await RssModel.findByIdAndDelete(req.params.id);
    if (!rss) res.status(404).send("No item found");
    return success(res, 'deleted successfully');
  } catch (err) {
    return error(res, err.message, 400)
  }
};

const rss_update = async (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  const { body } = req;
  try {
    let rss = await RssModel.findByIdAndUpdate(req.params.id, req.body);
    await rss.save();
    return success(res, rss);
  } catch (err) {
    return error(res, err.message, 400)
  }
};

exports.find_all = find_all;
exports.find_one = find_one;
exports.rss_save = rss_save;
exports.rss_delete = rss_delete;
exports.rss_update = rss_update;