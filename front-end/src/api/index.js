import axiosInstance from "./axiosInstance";

export default {
  findAll() {
    return new Promise((resolve, reject) => {
      axiosInstance.get('/rss/find-all')
        .then(response => {
          resolve(response)
        })
        .catch(err => {
          reject(err);
        })
    })
  },

  getSite(siteData) {
    return new Promise((resolve, reject) => {
      axiosInstance.post('/rss/save', siteData)
        .then(response => {
          resolve(response)
        })
        .catch(err => {
          reject(err);
        })
    })
  }
}