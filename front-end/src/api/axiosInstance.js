import axios from "axios";

// api instance
export default axios.create({
  baseURL: "http://localhost:9000"
});
