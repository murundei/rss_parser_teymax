import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
// import logger from './utils/logger';
// import passport from 'passport';
// import { init as socketInit } from './utils/socket-io';

// db instance connection
require("./config/db");

const server = express();

const port = process.env.PORT || 9000;
server.use(cors());
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

// import rss routes
const rssRoutes = require('./routes/rssRoutes');
// register rss routes
server.use('/rss', rssRoutes);

server.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});

// const io = require('socket.io')(server);
