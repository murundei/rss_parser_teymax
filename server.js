import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
const pino = require('pino');
const expressPino = require('express-pino-logger');

const logger = pino({ level: process.env.LOG_LEVEL || 'info' });
const expressLogger = expressPino({ logger });

// import logger from './utils/logger';
import passport from 'passport';
// import { init as socketInit } from './utils/socket-io';

// db instance connection
require("./config/db");
require('dotenv').config();

const server = express();

server.use(expressLogger);
server.use(passport.initialize());

const path = require('path');
const fs = require('fs');

const port = process.env.PORT || 9000;
server.use(cors());
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

// import rss routes
const rssRoutes = require('./routes/rss.routes');
// import user routes
const userRoutes = require('./routes/user.routes');
// register user routes
server.use('/rss', rssRoutes);
// register rss routes
server.use('/user', userRoutes);

server.listen(port, () => {
  logger.info(`Server running at http://localhost:${port}`);
});

// const io = require('socket.io')(server);