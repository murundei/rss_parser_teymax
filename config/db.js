const mongoose = require("mongoose");
require('dotenv').config();

const dbURI = process.env.DATABASE_URI;

const options = {
  reconnectTries: Number.MAX_VALUE,
  poolSize: 10
};

mongoose.connect(dbURI, options).then(
  () => {
    console.log("Database connection established!");
  },
  err => {
    console.log("Error connecting Database instance due to: ", err);
  }
);

// require rss model
require("../models/rss.model");
require("../models/user.model");