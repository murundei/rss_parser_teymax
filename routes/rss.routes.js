const express = require('express');
const router = express.Router();
const RssController = require('../controllers/rss.controller');


router.get('/find-all', RssController.find_all);
router.post('/save', RssController.rss_save);
router.get('/find/:id', RssController.find_one);
router.delete('/delete/:id', RssController.rss_delete);
router.patch('/update/:id',RssController.rss_update);


module.exports = router;