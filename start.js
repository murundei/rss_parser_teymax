require("@babel/register")({
  presets: ["@babel/preset-env"],
  plugins: [
    ["@babel/plugin-transform-runtime",
      {
        "regenerator": true
      }
    ]
  ]
});

// Import the rest of our application.
console.log('babel works well!');
module.exports = require('./server.js');
