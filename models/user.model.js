const mongoose = require('mongoose');
import { throwError, to } from '../utils/requestHelpers';
import bcryptjs from 'bcryptjs';

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  refresh_token: {
    type: String,
  },
  rss: [ { type: mongoose.Schema.Types.ObjectId, ref: 'Rss' } ],
});

UserSchema.pre('save', async function(next) {
  let salt, hash, err;
  [err, salt] = await to(bcryptjs.genSalt(10));
  if (err) throwError(err.message, true);
  [err, hash] = await to(bcryptjs.hash(this.password, salt));
  if (err) throwError(err.message, true);
  this.password = hash;
  // everything is done, so let's call the next callback.
  next();
});

const User = mongoose.model("User", UserSchema);

User.prototype.comparePassword = async function (pw) {
  let err, pass;
  if(!this.password) throwError('password not set', true);
  [err, pass] = await to(bcryptjs.compare(pw, this.password));
  if(err) throwError(err.message, true);
  if(!pass) throwError('invalid password', true);
  return this;
};

module.exports = User;