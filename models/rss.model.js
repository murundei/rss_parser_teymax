const mongoose = require('mongoose');

const RssSchema = new mongoose.Schema({
  url: {
    type: String,
    required: true,
  },
  title: String,
  description: String,
  link: String,
  items: JSON,
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
});

const Rss = mongoose.model("Rss", RssSchema);
module.exports = Rss;